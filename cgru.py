import torch


class CGRU(torch.jit.ScriptModule):

    def __init__(self, input_size, hidden_size):
        super().__init__()

        self.input_size = input_size
        self.hidden_size = hidden_size
        self.w_inputs = torch.nn.Parameter(torch.Tensor(3 * hidden_size, input_size))
        self.w_hidden = torch.nn.Parameter(torch.Tensor(4 * hidden_size, hidden_size))
        self.b_inputs = torch.nn.Parameter(torch.Tensor(1, 3 * hidden_size))
        self.b_hidden = torch.nn.Parameter(torch.Tensor(1, 4 * hidden_size))

        self.__initialize_parameters()


    def __initialize_parameters(self): #kernel weights xavier_uniform, recurrent weights orthogonal, bias zeros, similar tf

        for weight in self.parameters():
            if weight.shape[0] == 3*self.hidden_size:
                torch.nn.init.xavier_uniform_(weight)
            elif weight.shape[0] == 4*self.hidden_size:
                torch.nn.init.orthogonal_(weight)
                torch.nn.init.normal_(weight[3 * self.hidden_size:], mean=0, std=0.1) #init coupling very small
            elif weight.shape[0] == 1: 
                torch.nn.init.zeros_(weight)
                weight.data[:, self.hidden_size: 2*self.hidden_size].fill_(-1.) # reset bias equal -1 (equiv forget gate in lstm equal 1)
        
    def forward(self, x_left, x_right, h_left=None, h_right=None):

        if h_left is None:
            h_left = torch.zeros(x_left.size()[0], self.hidden_size).to(next(self.parameters()).device) 

        if h_right is None:
            h_right = torch.zeros(x_right.size()[0], self.hidden_size).to(next(self.parameters()).device)

        o_left, o_right = [], []

        for i in range(x_left.size(1)):
            h_left, h_right = self.__cell(x_left[:, i, :], x_right[:, i, :], h_left, h_right)

            o_left.append(h_left)
            o_right.append(h_right)

        return torch.stack(o_left, 1), torch.stack(o_right, 1), h_left, h_right  # batch first, stack to have sequences 

    def __cell(self, x_left, x_right, h_left, h_right):

        gates_input_left = torch.addmm(self.b_inputs, x_left, self.w_inputs.transpose(0, 1))
        gates_hidden_left = torch.addmm(self.b_hidden, h_left, self.w_hidden.transpose(0, 1))
        gates_input_right = torch.addmm(self.b_inputs, x_right, self.w_inputs.transpose(0, 1))
        gates_hidden_right = torch.addmm(self.b_hidden, h_right, self.w_hidden.transpose(0, 1))
        gates_input_left = gates_input_left.chunk(3, 1)
        gates_hidden_left = gates_hidden_left.chunk(4, 1)
        gates_input_right = gates_input_right.chunk(3, 1)
        gates_hidden_right = gates_hidden_right.chunk(4, 1)

        update_gate_left = torch.sigmoid(gates_input_left[0] + gates_hidden_left[0])
        reset_gate_left = torch.sigmoid(gates_input_left[1] + gates_hidden_left[1])
        new_gate_left = torch.tanh(gates_input_left[2] + reset_gate_left * gates_hidden_left[2])

        update_gate_right = torch.sigmoid(gates_input_right[0] + gates_hidden_right[0])
        reset_gate_right = torch.sigmoid(gates_input_right[1] + gates_hidden_right[1])
        new_gate_right = torch.tanh(gates_input_right[2] + reset_gate_right * gates_hidden_right[2])

        #coupling
        coupling_gate = torch.sigmoid(gates_hidden_left[3] + gates_hidden_right[3])
        new_gate_left_coupled = ((1 - coupling_gate) * new_gate_left) + (coupling_gate * new_gate_right)
        new_gate_right_coupled = ((1 - coupling_gate) * new_gate_right) + (coupling_gate * new_gate_left)

        new_h_left = new_gate_left_coupled + update_gate_left * (h_left - new_gate_left_coupled)
        new_h_right = new_gate_right_coupled + update_gate_right * (h_right - new_gate_right_coupled)

        return new_h_left, new_h_right
    
