from sick_dataset import SICKDataset
import numpy as np
import pytorch_lightning as pl 
from pytorch_lightning.callbacks import EarlyStopping, ModelCheckpoint
from coupled_gru_reg import SiamCoupledGRU
from regression import lowess_regression

perform_test = True
regress_scores = True
dataset = SICKDataset(test=perform_test, batch_size=1024) #set test=true to load test data
# BEST batch_size=1024

valid_scores = {"mse":[], "pr":[], "sr":[]}
test_scores = {"mse":[], "pr":[], "sr":[], "mse_reg":[], "pr_reg":[], "sr_reg":[]}
for i in range(5):

    model = SiamCoupledGRU(euclidean=False, selector=True, d_out=50) 
    # lots of options for training (see doc)
    trainer = pl.Trainer(enable_checkpointing=True, gradient_clip_val=6, max_epochs=1000, #gpus=1,
    callbacks=[EarlyStopping(monitor="val_loss", mode="min", patience=20), ModelCheckpoint(monitor="val_loss", mode="min")])

    trainer.fit(model, dataset)
    valid_scores_i = trainer.test(model, dataset.val_dataloader())[0]  #replace valid_input by test_input to perform test

    for k in valid_scores:
        valid_scores[k].append(valid_scores_i[k])

    if perform_test:
        if regress_scores:
          test_scores_i = lowess_regression(model, dataset)
        else:
          test_scores_i = trainer.test(model, dataset)[0]

        for k in test_scores:
            test_scores[k].append(test_scores_i[k])

print("*** validation scores ***")
for k in valid_scores:
    print(k + " : ", np.mean(valid_scores[k]))

if perform_test:
    print("*** test scores ***")
    for k in test_scores:
        print(k + " : ", np.mean(test_scores[k]))

