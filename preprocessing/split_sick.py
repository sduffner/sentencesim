import pandas as pd

data = pd.read_csv("../datasets/sick/sick_pairs.df")

# from original downloaded data files
train_users = ["TRAIN", "TRIAL"]

train_data = data[data.split.isin(train_users)]
test_data = data[~data.split.isin(train_users)]

train_data.to_csv("../datasets/sick/sick_pairs_train.df", index=False)
test_data.to_csv("../datasets/sick/sick_pairs_test.df", index=False)
