import pandas as pd
import re


def replace_absent_words(sentence):
    if "aren't" in sentence:
        sentence = sentence.replace("aren't", "are not")
    if "isn't" in sentence:
        sentence = sentence.replace("isn't", "is not")
    if "uninterestedly" in sentence:
        sentence = sentence.replace("uninterestedly", "uninterestingly")
    if "midspeech" in sentence:
        sentence = sentence.replace("midspeech", "mid-speech")
    return sentence

def split_genitive(sentence):
    new = []
    for word in sentence: 
        if "'s" in word: 
            new.append(word[:-2])
            new.append("'s")
        else: 
            new.append(word)
    return new


sick_raw_data = pd.read_csv("../datasets/sick/SICK.txt", sep="\t")

sick_raw_data = sick_raw_data.drop(['entailment_label','entailment_AB', 'entailment_BA',
                                    'sentence_A_original', 'sentence_B_original', 'sentence_A_dataset',
                                    'sentence_B_dataset'], axis=1)
sentences = pd.DataFrame({"sentences": sick_raw_data["sentence_A"].tolist() + 
sick_raw_data["sentence_B"].tolist()})

sentences = sentences.drop_duplicates()
sentences["processed"] = sentences["sentences"].apply(
    lambda x: replace_absent_words(x))
sentences["processed"] = sentences["processed"].apply(
    lambda x: re.findall(r"[\w'-]+|[.,!?;]", x.lower()))
sentences["processed"] = sentences["processed"].apply(lambda x: split_genitive(x))
sentences = sentences.reset_index()

sentences["id"] = pd.Series([i for i in range(len(sentences))])
sentences = sentences.reset_index()
sentences_id = {i:j for i, j in zip(sentences["sentences"].tolist(), sentences["id"].tolist())}

sick_raw_data["id_A"] = sick_raw_data["sentence_A"].apply(lambda x : sentences_id[x])
sick_raw_data["id_B"] = sick_raw_data["sentence_B"].apply(
    lambda x: sentences_id[x])


sick_raw_data = sick_raw_data.drop(["sentence_A", "sentence_B"], axis=1)
sick_raw_data.set_axis(["pair_id", "score", "split", "id_A", "id_B"], axis=1, inplace=True)

sick_raw_data.to_csv("../datasets/sick/sick_pairs.df", index=False)

word_vectors = pd.read_csv("../datasets/sick/dico_sick.csv")
word_vectors.set_index("word", inplace=True)

sick_sequences = pd.DataFrame({i:[] for i in range(300)})
ids = []
counter = 0
for s in sentences.index:

    ids+= [sentences["id"][s]]*len(sentences["processed"][s])
    for word in sentences["processed"][s]:
        sick_sequences.loc[counter] = word_vectors.loc[word].tolist()
        counter +=1

sick_sequences["id"] = pd.Series(ids)
sick_sequences.to_csv("../datasets/sick/sick_sequences.csv", index=False)
