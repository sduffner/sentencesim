import pandas as pd
import re

def replace_absent_words(sentence):
    if "aren't" in sentence: 
        sentence = sentence.replace("aren't", "are not")
    if "isn't" in sentence:
        sentence = sentence.replace("isn't", "is not")
    if "uninterestedly" in sentence:
        sentence = sentence.replace("uninterestedly", "uninterestingly")
    if "midspeech" in sentence:
        sentence = sentence.replace("midspeech", "mid-speech")
    return sentence

sick_raw_data = pd.read_csv("../datasets/sick/SICK.txt", sep="\t")

sentences = pd.DataFrame({"sentences": sick_raw_data["sentence_A"].tolist() +
                          sick_raw_data["sentence_B"].tolist()})

sentences = sentences.drop_duplicates()

sentences["sentences"] = sentences["sentences"].apply(lambda x : replace_absent_words(x)) 
sentences["processed"] = sentences["sentences"].apply(
    lambda x: set(re.findall(r"[\w'-]+|[.,!?;]", x.lower())))

dico = set(list(set.union(*sentences["processed"].tolist())))

dico2 = []
for word in dico: 
    if "'s" in word: 
        w = word[:-2]
        dico2.append(w)
    else:
        dico2.append(word)
dico2.append("'s")
dico = set(dico2)

word_vectors = pd.DataFrame({i:[] for i in range(300)})
word_vectors.insert(0, "word", 0)
dico_quick = {i:0 for i in dico}

with open("../datasets/sick/glove.42B.300d.txt", "r") as f:
    i = 0
    for line in f:
        tokens = line.split()
        if tokens[0] in dico_quick:
            to_insert = []
            to_insert.append(tokens[0])

            for t in tokens[1:]:
                to_insert.append(float(t))
            word_vectors.loc[i] = to_insert
            i +=1

            dico_quick.pop(tokens[0])
        if len(dico) == 0:
            break

print(dico_quick)
word_vectors.to_csv("../datasets/sick/dico_sick.csv", index=False)
