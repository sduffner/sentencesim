import pandas as pd
from sklearn.preprocessing import MinMaxScaler
from torch.utils.data import TensorDataset, DataLoader
import random
import pytorch_lightning as pl
import torch
from torch.nn.utils.rnn import pad_sequence


class SICKDataset(pl.LightningDataModule):

    def __init__(self, data_dir = "datasets/sick/", batch_size=50, test = False):

        self.train_file = data_dir + "sick_pairs_train.df"
        self.test_file = data_dir + "sick_pairs_test.df"
        self.sequence_file = data_dir + "sick_sequences.csv"
        self.batch_size = batch_size
        self.features = [str(i) for i in range(300)]
        self.columns = ["pair_id", "score", "split", "id_A", "id_B"]
        self.scaler = MinMaxScaler()
        self.test=test
        self.train_dataset = None
        self.sequences = None
        self.test_pairs = None

    def prepare_data(self):
        pass

    def setup(self, stage):
        
        self.train_dataset = pd.read_csv(self.train_file, usecols=self.columns) # only pairs of id
        sequences_data = pd.read_csv(self.sequence_file)
        self.sequences = {}
        for gr, df in sequences_data.groupby(['id']):
            self.sequences[gr] = torch.Tensor(df[self.features].to_numpy()) # dico of sequences indexed by id 

        self.train_dataset["score"] = self.scaler.fit_transform(self.train_dataset[["score"]]) # pass score between 0 and 1

        if self.test:
            self.test_pairs = pd.read_csv(self.test_file, usecols=self.columns)
            self.test_pairs["score"] = self.scaler.transform(self.test_pairs[["score"]])


    def train_dataloader(self):

        train_sequence_pairs = []
        train_pairs = self.train_dataset[self.train_dataset["split"]== "TRAIN"]
        
        for id_A, id_B in zip(train_pairs.id_A.tolist(), train_pairs.id_B.tolist()):
            
            pair = pad_sequence((self.sequences[id_A], self.sequences[id_B]), batch_first=True, padding_value=0) #first padding for the pair
            train_sequence_pairs.append(torch.cat(torch.unbind(pair), -1))     #then unbind the batch dimension to bind the feature dimension to match input size

        train_sequence_pairs = pad_sequence(train_sequence_pairs, batch_first=True, padding_value=0)
        train_scores = torch.Tensor(train_pairs.score.to_numpy())

        return DataLoader(TensorDataset(train_sequence_pairs, train_scores), batch_size=self.batch_size, shuffle=True, num_workers=80)


    def val_dataloader(self):
        valid_sequence_pairs = []
        valid_pairs = self.train_dataset[self.train_dataset["split"]== "TRIAL"]
        
        for id_A, id_B in zip(valid_pairs.id_A.tolist(), valid_pairs.id_B.tolist()):
            
            pair = pad_sequence((self.sequences[id_A], self.sequences[id_B]), batch_first=True, padding_value=0) #first padding for the pair
            valid_sequence_pairs.append(torch.cat(torch.unbind(pair), -1))     #then unbind the batch dimension to bind the feature dimension to match input size

        valid_sequence_pairs = pad_sequence(valid_sequence_pairs, batch_first=True, padding_value=0)
        valid_scores = torch.Tensor(valid_pairs.score.to_numpy())

        return DataLoader(TensorDataset(valid_sequence_pairs, valid_scores), batch_size=self.batch_size, num_workers=80)


    def test_dataloader(self):
        test_sequence_pairs = []
        
        for id_A, id_B in zip(self.test_pairs.id_A.tolist(), self.test_pairs.id_B.tolist()):
            
            pair = pad_sequence((self.sequences[id_A], self.sequences[id_B]), batch_first=True, padding_value=0) #first padding for the pair
            test_sequence_pairs.append(torch.cat(torch.unbind(pair), -1))     #then unbind the batch dimension to bind the feature dimension to match input size

        test_sequence_pairs = pad_sequence(test_sequence_pairs, batch_first=True, padding_value=0)
        test_scores = torch.Tensor(self.test_pairs.score.to_numpy())

        return DataLoader(TensorDataset(test_sequence_pairs, test_scores), batch_size=self.batch_size)
    
