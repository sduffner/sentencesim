from sklearn.linear_model import LinearRegression
from scipy.stats import pearsonr, spearmanr
from sklearn.metrics import mean_squared_error    
import statsmodels.nonparametric.smoothers_lowess as sm
from scipy.interpolate import interp1d
import numpy as np
from sick_dataset import SICKDataset

#perform_test = True
#dataset = SICKDataset(test=perform_test, batch_size=1024)
#dataset.setup('fit')

def lowess_regression(model, dataset):
  true_labels = np.array([])
  predictions = np.array([])
  for data, labels in dataset.train_dataloader():
    true_labels = np.append(true_labels, labels.detach().numpy())
    y = model(data).detach().numpy()
    predictions = np.append(predictions, y)
  for data, labels in dataset.val_dataloader():
    true_labels = np.append(true_labels, labels.detach().numpy())
    y = model(data).detach().numpy()
    predictions = np.append(predictions, y)

  #reg_labels = LinearRegression().fit(predictions.reshape(-1,1), true_labels)
  yhat = sm.lowess(true_labels, predictions, frac=1/3)  
  f_linear = interp1d(yhat[:,0], y=yhat[:,1], bounds_error=False, kind='linear', fill_value='extrapolate')

  test_labels = np.array([])                        
  pred_labels = np.array([])
  pred_labels_reg = np.array([])
  for data, y_true in dataset.test_dataloader():
    test_labels = np.append(test_labels, y_true.detach().numpy())
    y = model(data).detach().numpy()
    #y_reg = reg_labels.predict(y.reshape(-1,1))  # linear regression
    y_reg = f_linear(y.reshape(-1,1))  # lowess
    pred_labels = np.append(pred_labels, y)
    pred_labels_reg = np.append(pred_labels_reg, y_reg)


  mse = mean_squared_error(test_labels, pred_labels) * 16  # here we report the MSE w.r.t. the original scores in [1..5], thus there is a factor of 4 that gets squared in the MSE
  pr = pearsonr(test_labels, pred_labels)[0]
  sr = spearmanr(test_labels, pred_labels)[0]
  mse_reg = mean_squared_error(test_labels, pred_labels_reg) * 16 
  pr_reg = pearsonr(test_labels, pred_labels_reg)[0]
  sr_reg = spearmanr(test_labels, pred_labels_reg)[0]

  return {"mse":mse, "pr":pr, "sr":sr, "mse_reg":mse_reg, "pr_reg":pr_reg, "sr_reg":sr_reg}

