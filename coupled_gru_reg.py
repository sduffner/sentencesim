import os
import sys

import numpy as np
from scipy.stats import pearsonr, spearmanr
from sklearn.metrics import mean_squared_error
import torch
from torch.nn.utils.rnn import pad_sequence
from cgru import CGRU
import pytorch_lightning as pl
#from robust_loss_pytorch import lossfun
import torchsort

def spearmanr2(pred, target, **kw):
    pred = torchsort.soft_rank(pred, **kw)
    target = torchsort.soft_rank(target, **kw)
    pred = pred - pred.mean()
    pred = pred / pred.norm()
    target = target - target.mean()
    target = target / target.norm()
    return (pred * target).sum()


class Selector(torch.nn.Module):

    def __init__(self):
        super(Selector, self).__init__()

    def forward(self, x, dim):
        n = torch.norm(x, dim=dim).unsqueeze(dim)
        out = (n**2) / (n**2 + 0.01)
        return out


class SiamCoupledGRU(pl.LightningModule):

    def __init__(self, d_data=300, d_out=50, arch=[50], selector=True, euclidean=False):
        
        super().__init__()
        
        self.use_gru = False
        self.remove_trailing_zeros = True
        self.mae = torch.nn.L1Loss()
        self.d_data = d_data
        self.d_out = d_out
        self.d_entry = 100
        self.entry_layer = torch.nn.GRU(self.d_data, self.d_entry, batch_first=True, bidirectional=False)

        self.net = torch.nn.ModuleList()
        prev_layer_out_dim = self.d_entry   # 2*self.d_entry for Bi-GRU

        for hidden_layer in arch:
          if self.use_gru:
            self.net.append(torch.nn.GRU(prev_layer_out_dim, hidden_layer, batch_first=True)) 
          else:
            self.net.append(CGRU(prev_layer_out_dim, hidden_layer))
          prev_layer_out_dim = hidden_layer

        self.output_layer = torch.nn.Linear(prev_layer_out_dim, self.d_out, bias=False)

        if euclidean:
            self.similarity = lambda x, y: (1/(torch.nn.functional.pairwise_distance(x, y) + 1))      
            #self.similarity = lambda x, y: (1.0-torch.nn.functional.pairwise_distance(x, y))      
        else:
            self.similarity = lambda x, y: torch.exp(-torch.sum(torch.abs(x - y), dim = 1)) # BEST
            #self.similarity = lambda x, y: torch.exp(-torch.sum(torch.abs(x - y), dim = 1)/torch.mean(torch.stack((torch.sum(x!=0, axis=1).float(), torch.sum(y!=0, axis=1).float()), 1), dim=1))  # not working
            #self.similarity = lambda x, y: torch.nn.functional.cosine_similarity(x,y)

        if selector:
            self.s = Selector()
            def forward(self, inputs): 
                if self.remove_trailing_zeros:
                  allzero=inputs.shape[1]
                  for f in range(inputs.shape[1]-1,0,-1):
                    if not (inputs[:,f,:]==0).all():
                      allzero=f+1  # sequence is zero from allzero onwards
                      break
                  if allzero<inputs.shape[1]:
                    inputs = inputs[:,0:(allzero+1),:]

                x_left, x_right = inputs.chunk(2,2)
                y_left, _ = self.entry_layer(x_left)
                y_right, _ = self.entry_layer(x_right)


                for hidden_layer in self.net:
                  if self.use_gru:
                    y_left, h_left = hidden_layer(y_left) # GRU
                    y_right, h_right = hidden_layer(y_right) # GRU
                    y_left = torch.squeeze(y_left) # for GRU there is an additional 1st dimension (=1)  (batch size should be != 1)
                    y_right = torch.squeeze(y_right) # for GRU there is an additional 1st dimension (=1)  (batch size should be != 1)

                  else:
                    y_left, y_right, h_left, h_right  = hidden_layer(y_left, y_right) # CGRU

                #y_left = torch.sum(self.s(x_left, dim=2) * y_left, dim=1) # PC version
                #y_right = torch.sum(self.s(x_right, dim=2) * y_right, dim=1) # PC version
                #y_left = torch.sum( y_left, dim=1) # CGRU use all outputs
                #y_right = torch.sum( y_right, dim=1) # CGRU use all outputs
                #y_left = torch.max( y_left, dim=1)[0] # CGRU use all outputs
                #y_right = torch.max( y_right, dim=1)[0] # CGRU use all outputs
                y_left = torch.logsumexp( y_left, dim=1) # CGRU use all outputs
                y_right = torch.logsumexp( y_right, dim=1)# CGRU use all outputs
                #y_left = torch.logsumexp(self.s(x_left, dim=2) * y_left, dim=1) # CGRU use all outputs
                #y_right = torch.logsumexp(self.s(x_right, dim=2) * y_right, dim=1)# CGRU use all outputs
                #y_left = h_left  # CGRU only last hidden state
                #y_right = h_right  # CGRU only last hidden state

                y_left, y_right = self.output_layer(y_left), self.output_layer(y_right)
                return self.similarity(torch.nn.functional.normalize(y_left), torch.nn.functional.normalize(y_right))
                #return self.similarity(y_left, y_right)
        else:
            def forward(self, inputs):

                x_left, x_right = inputs.chunk(2,2)
                y_left, _ = self.entry_layer(x_left)
                y_right, _ = self.entry_layer(x_right)
                
                for hidden_layer in self.net:
                    y_left, y_right, h_left, h_right = hidden_layer(y_left, y_right)
   
                y_left, y_right = self.output_layer(h_left), self.output_layer(h_right)
 
                return self.similarity(torch.nn.functional.normalize(y_left), torch.nn.functional.normalize(y_right))

        setattr(self, "forward", forward.__get__(self))

    def configure_optimizers(self):
        optimizer = torch.optim.Adam(self.parameters(), lr=0.001, weight_decay=0.0001, amsgrad=True) # BEST
        #optimizer = torch.optim.Adagrad(self.parameters(), lr=0.001, weight_decay=0.0001) 
        #optimizer = torch.optim.Adam(self.parameters(), lr=0.0001, weight_decay=0.0001, amsgrad=True)
        #scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(optimizer, factor=0.1) 
        #optimizer = torch.optim.SGD(self.parameters(), lr=0.1, weight_decay=0.0001, momentum=0.9)
        #scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(optimizer, factor=0.01) # BEST
        scheduler = torch.optim.lr_scheduler.ExponentialLR(optimizer, gamma=0.95) # BEST
        return {"optimizer": optimizer, "lr_scheduler":scheduler, "monitor":"train_loss"}
        #return {"optimizer": optimizer, "monitor":"train_loss"}

    def training_step(self, batch):
        
        input_pairs, targets = batch
        dists = self(input_pairs)


        mse = torch.nn.functional.mse_loss(dists, targets)
        l1 = self.mae(dists, targets)
        #robust_loss = torch.sum(lossfun(dists-targets, torch.tensor(-2.0), torch.tensor(0.1)))
        spearman = spearmanr2(torch.unsqueeze(dists, dim=0), torch.unsqueeze(targets, dim=0))

        #loss = -0.05*spearman+l1
        loss = -spearman+l1 # BEST
        #loss = l1
        #loss = -spearman
        self.log("train_loss", loss)
        return loss

    def validation_step(self, eval_data, batch_idx):

        x, y_true = eval_data
        dists = self(x)

        mse = torch.nn.functional.mse_loss(dists, y_true)
        l1 = self.mae(dists, y_true)
        spearman = spearmanr2(torch.unsqueeze(dists, dim=0), torch.unsqueeze(y_true, dim=0))
        pr = pearsonr(y_true.numpy().flatten(), dists.numpy().flatten())[0]
        sr = spearmanr(y_true.numpy().flatten(), dists.numpy().flatten())[0]

        #loss = -0.05*spearman+l1
        loss = -spearman+l1 # BEST
        #loss = l1
        #loss = -spearman

        self.log("val_loss", loss)
        self.log("val_pr", pr)
        self.log("val_sr", sr)


    def test_step(self, data, batch_idx):

        x, y_true = data
        dists = self(x)

        mse = mean_squared_error(y_true, dists)
        #mse = self.mae(y_true, dists)
        pr = pearsonr(y_true.numpy().flatten(), dists.numpy().flatten())[0]
        sr = spearmanr(y_true.numpy().flatten(), dists.numpy().flatten())[0]
        self.log("mse", mse)
        self.log("pr", pr)
        self.log("sr", sr)

    def transfer_to_gpu(self):
        self.to("gpu:0")
